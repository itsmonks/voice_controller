import time
import keypress as kp

mappings = {
    "go"      : kp.W,
    "back"    : kp.S,
    "left"    : kp.A,
    "laughed" : kp.A,
    "laugh" : kp.A,
    "right"   : kp.D,
    "boost"   : kp.X,
    "skip"    : kp.Z,
    "jump"    : kp.Z,
    "slide"   : kp.M,
    "roll"    : kp.M
}

def score():
    kp.PressKey(kp.TAB)
    time.sleep(2)
    kp.ReleaseKey(kp.TAB)

def releaseAllKeys():
    kp.ReleaseKey(kp.W)
    kp.ReleaseKey(kp.S)
    kp.ReleaseKey(kp.A)
    kp.ReleaseKey(kp.X)
    kp.ReleaseKey(kp.Z)
    kp.ReleaseKey(kp.M)

def trashtalk():
    kp.PressKey(kp.T)
    time.sleep(3)
    kp.PressKey(kp.U)
    kp.PressKey(kp.SPACE)
    kp.PressKey(kp.S)
    kp.PressKey(kp.U)
    kp.PressKey(kp.C)
    kp.PressKey(kp.K)
    kp.PressKey(kp.ENTER)

def execute_rl_command(command):
    if command == "stop":
        releaseAllKeys()
        print("Release all keys")
    elif command == "trash":
        releaseAllKeys()
        trashtalk()
    elif command == "score":
        score()
    elif command in mappings:
        kp.PressKey(kp.W)
        kp.PressKey(mappings[command])
        time.sleep(1)
        kp.ReleaseKey(mappings[command])
    

