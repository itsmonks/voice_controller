import speech_recognition as sr
from commands import execute_rl_command

def listen():
    r = sr.Recognizer()
    print("Listening")
    while(True):
        with sr.Microphone() as source:
            audio_text = r.listen(source, phrase_time_limit=1)    
            try:
                sentence = r.recognize_google(audio_text)
                print(sentence)
                execute_rl_command(sentence)
            except:
                continue

if __name__ == "__main__":
    listen()